package ru.vkandyba.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.enumerated.Status;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name = "app_projects")
public class Project extends AbstractBusinessEntity {

    @Column
    private String name;

    @Column
    private String description;

    @Enumerated(EnumType.STRING)
    private Status status = Status.NON_STARTED;

    @Column(name = "start_date")
    private Date startDate;

    @Column(name = "finish_date")
    private Date finishDate;

    @Column(name = "created_date")
    private Date createdDate = new Date();

    public Project(String name, String description) {
        this.name = name;
        this.description = description;
    }

    @Nullable
    @OneToMany(mappedBy = "project")
    private List<Task> tasks = new ArrayList<>();

    @Override
    public String toString() {
        return id + ": " + name;
    }

}
