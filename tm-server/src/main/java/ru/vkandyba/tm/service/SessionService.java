package ru.vkandyba.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.repository.ISessionRepository;
import ru.vkandyba.tm.api.service.*;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.exception.system.AccessDeniedException;
import ru.vkandyba.tm.dto.SessionDTO;
import ru.vkandyba.tm.dto.UserDTO;
import ru.vkandyba.tm.util.HashUtil;

public class SessionService extends AbstractService<SessionDTO> implements ISessionService {

    @NotNull
    final IUserService userService;

    public SessionService(@NotNull IConnectionService connectionService, @NotNull IUserService userService) {
        super(connectionService);
        this.userService = userService;
    }

    @SneakyThrows
    @Nullable
    @Override
    public SessionDTO open(
            @Nullable final String login,
            @Nullable final String password
    ) {
        final boolean check = checkDataAccess(login, password);
        if (!check) return null;
        final UserDTO user = userService.findByLogin(login);
        if (user == null) return null;
        @NotNull final SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setUserId(user.getId());
        sessionDTO.setTimestamp(System.currentTimeMillis());
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            repository.add(sessionDTO);
            sqlSession.commit();
            return sign(sessionDTO);
        } catch (Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }


    @SneakyThrows
    @Override
    public void close(@Nullable final SessionDTO sessionDTO) {
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            repository.removeById(sessionDTO.getId());
        } catch (Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public boolean checkDataAccess(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login.isEmpty() || password.isEmpty()) return false;
        final UserDTO user = userService.findByLogin(login);
        if (user == null) return false;
        final String passwordHash = HashUtil.salt(connectionService.getPropertyService(), password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    @SneakyThrows
    @Override
    public void validate(SessionDTO sessionDTO) {
        if(sessionDTO == null) throw new AccessDeniedException();
        if(sessionDTO.getSignature() == null || sessionDTO.getSignature().isEmpty()) throw new AccessDeniedException();
        if(sessionDTO.getUserId() == null || sessionDTO.getUserId().isEmpty()) throw new AccessDeniedException();
        if(sessionDTO.getTimestamp() == null) throw new AccessDeniedException();
        @NotNull final SessionDTO temp = sessionDTO.clone();
        @NotNull final String signatureSource = sessionDTO.getSignature();
        @NotNull final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if(!check) throw new AccessDeniedException();
    }

    @Override
    public void validate(SessionDTO sessionDTO, Role role) {
        validate(sessionDTO);
        @Nullable final UserDTO user = userService.findById(sessionDTO.getUserId());
        if(user == null) throw new AccessDeniedException();
        if(!role.equals(user.getRole())) throw new AccessDeniedException();
    }

    public @Nullable SessionDTO sign(SessionDTO sessionDTO) {
        if(sessionDTO == null) return null;
        sessionDTO.setSignature(null);
        @Nullable final String signature = HashUtil.sign(connectionService.getPropertyService(), sessionDTO);
        sessionDTO.setSignature(signature);
        return sessionDTO;
    }

}
