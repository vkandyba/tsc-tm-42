package ru.vkandyba.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.endpoint.IProjectEndpoint;
import ru.vkandyba.tm.api.service.ServiceLocator;
import ru.vkandyba.tm.dto.ProjectDTO;
import ru.vkandyba.tm.dto.SessionDTO;

import javax.jws.WebService;
import java.util.List;

@WebService
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAllProjects(@NotNull SessionDTO sessionDTO) {
        serviceLocator.getSessionService().validate(sessionDTO);
        return serviceLocator.getProjectService().findAll(sessionDTO.getUserId());
    }

    @Override
    public void addProject(@NotNull SessionDTO sessionDTO, @NotNull ProjectDTO project) {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getProjectService().add(sessionDTO.getUserId(),project);
    }

    @Override
    public void finishProjectById(@NotNull SessionDTO sessionDTO, @NotNull String projectId) {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getProjectService().finishById(sessionDTO.getUserId(), projectId);
    }

    @Override
    public void finishProjectByName(@NotNull SessionDTO sessionDTO, @NotNull String name) {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getProjectService().finishByName(sessionDTO.getUserId(), name);
    }

    @Override
    public void removeProjectById(@NotNull SessionDTO sessionDTO, @NotNull String projectId) {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getProjectService().removeById(sessionDTO.getUserId(), projectId);
    }

    @Override
    public void removeProjectByName(@NotNull SessionDTO sessionDTO, @NotNull String name) {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getProjectService().removeByName(sessionDTO.getUserId(), name);
    }

    @Override
    public ProjectDTO showProjectById(@NotNull SessionDTO sessionDTO, @NotNull String projectId) {
        serviceLocator.getSessionService().validate(sessionDTO);
        return serviceLocator.getProjectService().findById(sessionDTO.getUserId(), projectId);
    }

    @Override
    public void startProjectById(@NotNull SessionDTO sessionDTO, @NotNull String projectId) {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getProjectService().startById(sessionDTO.getUserId(), projectId);
    }

    @Override
    public void startProjectByName(@NotNull SessionDTO sessionDTO, @NotNull String name) {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getProjectService().startByName(sessionDTO.getUserId(), name);
    }

    @Override
    public void updateProjectById(@NotNull SessionDTO sessionDTO, @NotNull String projectId, @NotNull String name, @NotNull String description) {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getProjectService().updateById(sessionDTO.getUserId(), projectId, name, description);
    }

    @Override
    public void removeProjectWithTasksById(@NotNull SessionDTO sessionDTO, @NotNull String projectId) {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getProjectService().removeById(sessionDTO.getUserId(), projectId);
    }

    @Override
    public void clearProjects(@NotNull SessionDTO sessionDTO) {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getProjectService().clear(sessionDTO.getUserId());
    }

}
