package ru.vkandyba.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.endpoint.IUserEndpoint;
import ru.vkandyba.tm.api.service.ServiceLocator;
import ru.vkandyba.tm.dto.SessionDTO;
import ru.vkandyba.tm.dto.UserDTO;

import javax.jws.WebService;

@WebService
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public @Nullable UserDTO viewUserInfo(@NotNull SessionDTO sessionDTO) {
        serviceLocator.getSessionService().validate(sessionDTO);
        return serviceLocator.getUserService().findById(sessionDTO.getUserId());
    }

    @Override
    public void changeUserPassword(@NotNull SessionDTO sessionDTO, @NotNull String password) {
        serviceLocator.getSessionService().validate(sessionDTO);
//        serviceLocator.getUserService().setPassword(session.getUserId(), password);
    }

    @Override
    public void updateUserProfile(@NotNull SessionDTO sessionDTO, @NotNull String firstName, @NotNull String lastName, @NotNull String midName) {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getUserService().updateUser(sessionDTO.getUserId(), firstName, lastName, midName);
    }

}
