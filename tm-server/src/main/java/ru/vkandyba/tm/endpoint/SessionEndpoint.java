package ru.vkandyba.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.vkandyba.tm.api.endpoint.ISessionEndpoint;
import ru.vkandyba.tm.api.service.ServiceLocator;
import ru.vkandyba.tm.dto.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    public SessionEndpoint(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public SessionDTO openSession(
            @WebParam(name = "login", partName = "login") String login,
            @WebParam(name = "password", partName = "password") String password
    ) {
        return serviceLocator.getSessionService().open(login, password);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void closeSession(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO sessionDTO
    ) {
        serviceLocator.getSessionService().close(sessionDTO);
    }

}
