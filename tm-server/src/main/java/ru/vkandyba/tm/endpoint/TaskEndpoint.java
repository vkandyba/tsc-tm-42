package ru.vkandyba.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.endpoint.ITaskEndpoint;
import ru.vkandyba.tm.api.service.ServiceLocator;
import ru.vkandyba.tm.dto.TaskDTO;
import ru.vkandyba.tm.dto.SessionDTO;

import javax.jws.WebService;
import java.util.List;

@WebService
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllTasks(@NotNull SessionDTO sessionDTO) {
        serviceLocator.getSessionService().validate(sessionDTO);
        return serviceLocator.getTaskService().findAll(sessionDTO.getUserId());
    }

    @Override
    public void addTask(@NotNull SessionDTO sessionDTO, @NotNull TaskDTO task) {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getTaskService().add(sessionDTO.getUserId(),task);
    }

    @Override
    public void finishTaskById(@NotNull SessionDTO sessionDTO, @NotNull String taskId) {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getTaskService().finishById(sessionDTO.getUserId(), taskId);
    }

    @Override
    public void finishTaskByName(@NotNull SessionDTO sessionDTO, @NotNull String name) {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getTaskService().finishByName(sessionDTO.getUserId(), name);
    }

    @Override
    public void removeTaskById(@NotNull SessionDTO sessionDTO, @NotNull String taskId) {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getTaskService().removeById(sessionDTO.getUserId(), taskId);
    }

    @Override
    public void removeTaskByName(@NotNull SessionDTO sessionDTO, @NotNull String name) {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getTaskService().removeByName(sessionDTO.getUserId(), name);
    }

    @Override
    public TaskDTO showTaskById(@NotNull SessionDTO sessionDTO, @NotNull String taskId) {
        serviceLocator.getSessionService().validate(sessionDTO);
        return serviceLocator.getTaskService().findById(sessionDTO.getUserId(), taskId);
    }

    @Override
    public void startTaskById(@NotNull SessionDTO sessionDTO, @NotNull String taskId) {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getTaskService().startById(sessionDTO.getUserId(), taskId);
    }

    @Override
    public void startTaskByName(@NotNull SessionDTO sessionDTO, @NotNull String name) {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getTaskService().startByName(sessionDTO.getUserId(), name);
    }

    @Override
    public void updateTaskById(@NotNull SessionDTO sessionDTO, @NotNull String taskId, @NotNull String name, @NotNull String description) {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getTaskService().updateById(sessionDTO.getUserId(), taskId, name, description);
    }

    @Override
    public void removeTaskWithTasksById(@NotNull SessionDTO sessionDTO, @NotNull String taskId) {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getTaskService().removeById(sessionDTO.getUserId(), taskId);
    }

    @Override
    public void clearTasks(@NotNull SessionDTO sessionDTO) {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getTaskService().clear(sessionDTO.getUserId());
    }

    @Override
    public void unbindTaskFromProjectById(@NotNull SessionDTO sessionDTO, @NotNull String projectId, @NotNull String taskId) {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getProjectTaskService().unbindTaskToProjectById(sessionDTO.getUserId(), projectId, taskId);
    }

    @Override
    public @Nullable List<TaskDTO> showAllTasksByProjectId(@NotNull SessionDTO sessionDTO, @NotNull String projectId) {
        serviceLocator.getSessionService().validate(sessionDTO);
        return serviceLocator.getProjectTaskService().findAllTaskByProjectId(sessionDTO.getUserId(), projectId);
    }

    @Override
    public void bindTaskToProjectById(@NotNull SessionDTO sessionDTO, @NotNull String projectId, @NotNull String taskId) {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getProjectTaskService().bindTaskToProjectById(sessionDTO.getUserId(), projectId, taskId);
    }

}
