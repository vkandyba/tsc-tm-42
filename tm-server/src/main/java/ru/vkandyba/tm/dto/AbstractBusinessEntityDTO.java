package ru.vkandyba.tm.dto;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractBusinessEntityDTO extends AbstractEntityDTO {

    @Column(name = "user_id")
    protected String userId = null;

}
