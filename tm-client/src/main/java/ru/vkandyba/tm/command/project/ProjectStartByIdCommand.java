package ru.vkandyba.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.endpoint.Project;
import ru.vkandyba.tm.endpoint.Session;
import ru.vkandyba.tm.endpoint.SessionDTO;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.exception.entity.ProjectNotFoundException;
import ru.vkandyba.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectStartByIdCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "project-start-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Start project by id...";
    }

    @Override
    public void execute() {
        @NotNull final SessionDTO session = serviceLocator.getSession();
        System.out.println("Enter id");
        @Nullable final String id = TerminalUtil.nextLine();
        serviceLocator.getProjectEndpoint().startProjectById(session, id);
    }

    @NotNull
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
