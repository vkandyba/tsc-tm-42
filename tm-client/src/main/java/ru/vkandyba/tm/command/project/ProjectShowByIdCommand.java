package ru.vkandyba.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.endpoint.Project;
import ru.vkandyba.tm.endpoint.ProjectDTO;
import ru.vkandyba.tm.endpoint.Session;
import ru.vkandyba.tm.endpoint.SessionDTO;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.exception.entity.ProjectNotFoundException;
import ru.vkandyba.tm.util.TerminalUtil;

import java.util.List;
import java.util.Optional;

public class ProjectShowByIdCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "project-show-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project by id...";
    }

    @Override
    public void execute() {
        @NotNull final SessionDTO session = serviceLocator.getSession();
        System.out.println("Enter Id");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final ProjectDTO project = serviceLocator.getProjectEndpoint().showProjectById(session, id);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        @NotNull final List<ProjectDTO> projects = serviceLocator.getProjectEndpoint().findAllProjects(session);
        System.out.println("Index: " + projects.indexOf(project));
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + project.getStatus());
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
